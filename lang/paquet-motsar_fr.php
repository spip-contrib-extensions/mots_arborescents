<?php
return [
	// M
	'motsar_description' => 'Ce plugin permet d\'indiquer qu\'un groupe de mot peut recevoir des mots clés ayant une arborescence. Dans un tel groupe donc, il devient possible de mettre des mots, dans des mots, dans des mots…

Note : incompatible avec le plugin «Groupe de mots arborescents» (car ils surchargent tous les deux les mêmes fichiers du plugin «Mots»).',
	'motsar_nom' => 'Mots arborescents',
	'motsar_slogan' => 'Permet d\'organiser des mots-clés de façon arborescente',
];